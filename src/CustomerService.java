import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @Author Reshma
 * @Date 6/26/2018
 * @Month 06
 **/
public class CustomerService implements InitializingBean,DisposableBean {
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    String msg;

    @Override
    public void afterPropertiesSet() {
        System.out.println("Initializing message:"+msg);
    }

    @Override
    public void destroy() {
        System.out.println("This is destroy method");
    }
}
