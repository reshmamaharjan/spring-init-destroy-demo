import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author Reshma
 * @Date 6/26/2018
 * @Month 06
 **/
public class MainApp {
    public static void main(String[] args) {
        AbstractApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext.xml");
        CustomerService c=(CustomerService)ac.getBean("message");
        ac.registerShutdownHook();//call destroy method
    }


}
